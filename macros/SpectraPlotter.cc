#include "src/Common.h"
#include "src/Plotting.h"
#include "src/Utils.h"
using namespace utils;

#include <map>
#include <vector>
#include <array>
using std::array;
#include <memory>

#include <Riostream.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TList.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TLine.h>
#include <TF1.h>

#include <AliPID.h>
#include <AliPWGFunc.h>

#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooDataSet.h>
#include <RooPlot.h>
#include <RooGaussian.h>
#include <TVirtualFitter.h>
#include <TPaveText.h>
#include <TLatex.h>

const char* kPrefix[2] = {"deuterons","antideuterons"};

const char* kParticleNames[2] = {"Deuterons", "Antideuterons"};

void SpectraPlotter() {
  TFile spectra_file(kSpectraOutput.data());
  TFile final_file("../results/spectra_plot.root","recreate");

  //histograms with corrected spectra
  TH1F* stat[2][kCentLength];

  for (int iS = 0; iS < 2; ++iS) {
    TDirectory* s_dir = final_file.mkdir(kNames[iS].data());
    for (int iC = 0; iC < kCentLength; ++iC) {
      TDirectory *c_dir = s_dir->mkdir(std::to_string(iC).data());
      c_dir->cd();
      string basepath = kFilterListNames + "/" + kNames[iS] + "/" + std::to_string(iC) + "/Joined/JoinedSpectra" + kLetter[iS] + std::to_string(iC);
      stat[iS][iC] = (TH1F*)spectra_file.Get(basepath.data());
      Requires(stat[iS][iC],basepath.data());
      stat[iS][iC]->Write("stat");
      stat[iS][iC]->Scale(kScaleFactor[iC]);
    }

    TCanvas spectra("spectra","spectra",800,600);
		spectra.SetLogy();
    TH1* hFrame = spectra.DrawFrame(
        0.4,
        1e-5,
        6.,
        0.018,
        ";#it{p}_{T} (GeV/#it{c});#frac{1}{#it{N}_{ev}} #frac{d^{2}#it{N}}{d#it{p}_{T}d#it{y}} (GeV/#it{c})^{-1}"
        );
    spectra.SetLeftMargin(0.15);
    spectra.SetRightMargin(0.03);
    spectra.SetTopMargin(0.1);
    spectra.SetBottomMargin(0.14);
    hFrame->GetYaxis()->SetTitleOffset(1.3);
    TLatex text;
    text.SetTextFont(63);
    text.SetTextSize(22);
    //text.DrawText(0.5,7.5,"This work");
    float name_position = (iS==0) ? 3.5 : 3.2;
    text.DrawLatex(name_position,0.02,Form("#bf{%s, pp, #sqrt{#it{s}} = 13 TeV, HM}",kNamePlot[iS].data()));
    TLegend final_leg(0.71,0.54,0.95,0.87);
    final_leg.SetBorderSize(0);
    final_leg.SetTextSize(0.027);
    final_leg.SetHeader("V0M Multiplicity Classes");
    TLegendEntry *header = (TLegendEntry*)final_leg.GetListOfPrimitives()->First();
    header->SetTextAlign(22);
    for (int iC = 0; iC < kCentLength; ++iC) {
      stat[iS][iC]->Draw("esamex0");
      final_leg.AddEntry(stat[iS][iC],Form("%s - %s %% (#times 2^{ %d})",kCentLabelsChar[iC][0],kCentLabelsChar[iC][1],kExponent[iC]),"fp");
    }
    final_leg.Draw();
    s_dir->cd();
    spectra.Write();
  }
}
