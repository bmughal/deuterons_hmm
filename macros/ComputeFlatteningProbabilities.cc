#include "src/Common.h"
#include "src/Utils.h"
using namespace utils;
#include <TFile.h>
#include <TH2D.h>
#include <TH1D.h>

void ComputeFlatteningProbabilities(){
	TFile input_file(kDataFilename.data());
	TFile output_file(Form("%s/Normalisation.root", kBaseOutputDir.data()),"recreate");
	
	TTList* list = (TTList*)input_file.Get(kFilterListNames.data());
	TH2D* hNorm2D = (TH2D*)list->Get("fNormalisationHist");
	TH1D* hSelectedNorm = (TH1D*)hNorm2D->ProjectionX("hNormSelected",4,4);

	output_file.cd();
	hNorm2D->Write();
	hSelectedNorm->Write();
}
